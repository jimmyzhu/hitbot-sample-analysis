## Setup

### Prerequisites (Mac OS X)
[Install Homebrew][1] package manager
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

[Install pyenv][2] and [python build dependencies][3]
```
brew update
brew install pyenv openssl readline sqlite3 xz zlib

echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bash_profile
```

Restart your shell (close and reopen Terminal)

Install TK (GUI toolkit)
```
brew install tcl-tk
```

Use pyenv to install python 3.7.2 [with TK support][4]
```
PYTHON_CONFIGURE_OPTS="--with-tcltk-includes='-I/usr/local/opt/tcl-tk/include' --with-tcltk-libs='-L/usr/local/opt/tcl-tk/lib -ltcl8.6 -ltk8.6'" pyenv install 3.7.2
```

Set your global python version
```
pyenv global 3.7.2
```

Test your python version - if you see errors,
you might need to [install xcode command line tools][3]
```
python --version
python -m tkinter -c 'tkinter._test()'
```

Upgrade pip, install pipenv
```
pip install --upgrade pip
pip install pipenv
```

Install git version control
```
brew install git
```

[1]: https://brew.sh/
[2]: https://github.com/pyenv/pyenv#homebrew-on-macos
[3]: https://github.com/pyenv/pyenv/wiki#suggested-build-environment
[4]: https://github.com/pyenv/pyenv/issues/1375#issuecomment-533182043

### Workspace setup
Clone repository
```
git clone https://bitbucket.org/jimmyzhu/hitbot-sample-analysis.git
```

Navigate to repository and install dependencies
```
cd hitbot-sample-analysis
pipenv install
```

## Development

### Run program
```
pipenv run python interface/main.py
```

### Architecture

* **interface/main.py** - Main entry point into the program.
  Creates a window and manages the event loop.

* **interface/components.py** - Defines custom components used in the main layout.
  Example: CheckboxGrid defines a labeled grid of checkboxes

### Dependencies

* [PySimpleGUI][5] - GUI framework used in this package

[5]: https://pysimplegui.readthedocs.io/en/latest/

### Python quick reference
* [Data structures](https://docs.python.org/3/tutorial/datastructures.html)
* [Type hints](https://docs.python.org/3/library/typing.html)
