from typing import Dict
from components import CheckboxGrid, CHECKBOX_KEY_DELIMITER
import PySimpleGUI as sg

sg.theme('Dark Blue')

table = sg.Table(values=[3*['']], headings=['Well', 'STD Level', 'STD Conc.'],
                 alternating_row_color='#1f3434')
layout = [[
    sg.Column(CheckboxGrid('standard', size=(12,8))),
    table,
]]

window = sg.Window('HitBot Sample', layout, font='Helvetica 12')

while True:
    event: str
    checked: Dict[str,bool]

    event, checked = window.read()
    if event is None:
        break

    if event.startswith('standard'):
        checked_well_ids = sorted(
            checkbox_key.split(CHECKBOX_KEY_DELIMITER)[-1]
            for checkbox_key in checked.keys()
            if checkbox_key.startswith('standard') and checked[checkbox_key]
        )
        table.Update([[well_id, '', ''] for well_id in checked_well_ids])

window.close()
