from typing import List, Tuple
import PySimpleGUI as sg

Layout = List[List[sg.Element]]
ROW_HEADERS = ' ABCDEFGHIJKLMNOPQRSTUVWXYZ'
CHECKBOX_KEY_DELIMITER = '.'

def CheckboxGrid(key: str, size: Tuple[int,int]) -> Layout:
    cols, rows = size
    return [[
        sg.Column(_checkbox_grid_column(c, rows, pad=(0,4), key=key),
                  element_justification='center', pad=(0,0))
        for c in range(cols+1)
    ]]

def _checkbox_grid_column(column_index, rows, pad, key) -> Layout:
    if column_index == 0:
        # row header column
        return [[sg.Text(ROW_HEADERS[r], pad=pad)] for r in range(rows+1)]
    else:
        column_header = [[sg.Text(f'{column_index:2d}', pad=pad)]]
        checkboxes = [
            [sg.Checkbox('', enable_events=True, pad=pad,
                         key=f'{key}.{ROW_HEADERS[r]}{column_index}')]
            for r in range(1, rows+1)
        ]
        return column_header + checkboxes
